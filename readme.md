# Практические работы по дисциплине Тестирование и верификация ПО

Содержание:

* А1. Использование Redmine
* A2. Использование GitHub
* А3. Модульное тестирование в Java c jUnit
* А4. Модульное тестирование в Python c Unittest
* Б1. Развертывание Redmine
* Б2. Использование BitBucket

Лицензия: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
